import { owners, owner, createOwner, updateOwner, deleteOwner } from './owners'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('owners', () => {
  scenario('returns all owners', async (scenario) => {
    const result = await owners()

    expect(result.length).toEqual(Object.keys(scenario.owner).length)
  })

  scenario('returns a single owner', async (scenario) => {
    const result = await owner({ id: scenario.owner.one.id })

    expect(result).toEqual(scenario.owner.one)
  })

  scenario('creates a owner', async () => {
    const result = await createOwner({
      input: { name: 'String' },
    })

    expect(result.name).toEqual('String')
  })

  scenario('updates a owner', async (scenario) => {
    const original = await owner({ id: scenario.owner.one.id })
    const result = await updateOwner({
      id: original.id,
      input: { name: 'String2' },
    })

    expect(result.name).toEqual('String2')
  })

  scenario('deletes a owner', async (scenario) => {
    const original = await deleteOwner({ id: scenario.owner.one.id })
    const result = await owner({ id: original.id })

    expect(result).toEqual(null)
  })
})

import { db } from 'src/lib/db'

export const todoTasks = () => {
  return db.todoTask.findMany()
}

export const todoTask = ({ id }) => {
  return db.todoTask.findUnique({
    where: { id },
  })
}

export const createTodoTask = ({ input }) => {
  return db.todoTask.create({
    data: input,
  })
}

export const updateTodoTask = ({ id, input }) => {
  return db.todoTask.update({
    data: input,
    where: { id },
  })
}

export const deleteTodoTask = ({ id }) => {
  return db.todoTask.delete({
    where: { id },
  })
}

export const TodoTask = {
  owner: (_obj, { root }) =>
    db.todoTask.findUnique({ where: { id: root.id } }).owner(),
}

import {
  todoTasks,
  todoTask,
  createTodoTask,
  updateTodoTask,
  deleteTodoTask,
} from './todoTasks'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('todoTasks', () => {
  scenario('returns all todoTasks', async (scenario) => {
    const result = await todoTasks()

    expect(result.length).toEqual(Object.keys(scenario.todoTask).length)
  })

  scenario('returns a single todoTask', async (scenario) => {
    const result = await todoTask({ id: scenario.todoTask.one.id })

    expect(result).toEqual(scenario.todoTask.one)
  })

  scenario('creates a todoTask', async (scenario) => {
    const result = await createTodoTask({
      input: {
        task: 'String',
        done: true,
        ownerId: scenario.todoTask.two.ownerId,
      },
    })

    expect(result.task).toEqual('String')
    expect(result.done).toEqual(true)
    expect(result.ownerId).toEqual(scenario.todoTask.two.ownerId)
  })

  scenario('updates a todoTask', async (scenario) => {
    const original = await todoTask({ id: scenario.todoTask.one.id })
    const result = await updateTodoTask({
      id: original.id,
      input: { task: 'String2' },
    })

    expect(result.task).toEqual('String2')
  })

  scenario('deletes a todoTask', async (scenario) => {
    const original = await deleteTodoTask({ id: scenario.todoTask.one.id })
    const result = await todoTask({ id: original.id })

    expect(result).toEqual(null)
  })
})

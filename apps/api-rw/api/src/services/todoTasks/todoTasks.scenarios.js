export const standard = defineScenario({
  todoTask: {
    one: {
      data: {
        task: 'String',
        done: true,
        owner: { create: { name: 'String' } },
      },
    },

    two: {
      data: {
        task: 'String',
        done: true,
        owner: { create: { name: 'String' } },
      },
    },
  },
})

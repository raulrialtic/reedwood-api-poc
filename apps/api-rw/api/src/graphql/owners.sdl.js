export const schema = gql`
  type Owner {
    id: Int!
    name: String!
    task: [TodoTask]!
  }

  type Query {
    owners: [Owner!]! @requireAuth
    owner(id: Int!): Owner @requireAuth
  }

  input CreateOwnerInput {
    name: String!
  }

  input UpdateOwnerInput {
    name: String
  }

  type Mutation {
    createOwner(input: CreateOwnerInput!): Owner! @requireAuth
    updateOwner(id: Int!, input: UpdateOwnerInput!): Owner! @requireAuth
    deleteOwner(id: Int!): Owner! @requireAuth
  }
`

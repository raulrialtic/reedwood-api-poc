export const schema = gql`
  type TodoTask {
    id: Int!
    task: String!
    done: Boolean!
    createdAt: DateTime!
    owner: Owner!
    ownerId: Int!
  }

  type Query {
    todoTasks: [TodoTask!]! @requireAuth
    todoTask(id: Int!): TodoTask @requireAuth
  }

  input CreateTodoTaskInput {
    task: String!
    done: Boolean!
    ownerId: Int!
  }

  input UpdateTodoTaskInput {
    task: String
    done: Boolean
    ownerId: Int
  }

  type Mutation {
    createTodoTask(input: CreateTodoTaskInput!): TodoTask! @requireAuth
    updateTodoTask(id: Int!, input: UpdateTodoTaskInput!): TodoTask!
      @requireAuth
    deleteTodoTask(id: Int!): TodoTask! @requireAuth
  }
`

import { PrismaClient } from '@prisma/client'
import { owners } from '../api/db/seeds/owners'
import { tasks } from '../api/db/seeds/tasks'
const prisma = new PrismaClient()

async function main() {
  // Add some Owners
  for (const owner of owners) {
    await prisma.owner.create({
      data: {
        ...owner,
      },
    })
  }
  // Add some tasks
  for (const task of tasks) {
    await prisma.todoTask.create({
      data: {
        ...task,
      },
    })
  }
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })

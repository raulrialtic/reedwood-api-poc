# RwApi

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Application Commands**

## Before Running the application

Will be require to run the first time `npm run migrate` to create the DB (in this example is used SQLite) then you can start the project with `npm start`

## Run the application

Redwood is not very flexible outside his workflow, so to adapt funtionalities (temporal)

Run `npm start` to run the application. This will install dependencies in RW workspace (until look for a solution) and run the proyect in development mode

## Test the application

Run `npm run test` to run all the test in the api.

## Migrate and Seeding

Run `npm run seed` if you want to run only the seeding process or `npm run migrate` to build the DB Schema and also run the initial seeding.

## Building the application

Run `npm run build` to generate a build folder.

> Note: Pending to solve the position of the building files because of RW client default path
